import logging

from django.db import models
from onbasca.base.models.relaydesc import RelayDescBase, RelayDescManagerBase

from .relay import Relay

logger = logging.getLogger(__name__)


class RelayDescManager(RelayDescManagerBase):
    def from_relay_descs(self, document):
        # logger.info("Importing RelayDescs %s", len(document))
        for rd in document:
            # logger.debug("%s", rd)
            self.from_relay_desc(rd)
            # logger.info("Imported RelayBalanceRatio.")

    def from_relay_desc(self, relay_desc):
        relay, _ = Relay.objects.get_or_create(
            fingerprint=relay_desc.fingerprint
        )
        kwargs = {"relay": relay}
        rd = super().from_relay_desc(relay_desc, **kwargs)
        # logger.info("Created relay descriptor %s.", rd.fingerprint)
        return rd


class RelayDesc(RelayDescBase):
    objects = RelayDescManager()
    relay = models.ForeignKey(
        "Relay", on_delete=models.CASCADE, null=True, blank=True
    )
