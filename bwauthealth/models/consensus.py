import logging

from onbasca.base.models.consensus import ConsensusBase, ConsensusManagerBase

from .routerstatus import RouterStatus

logger = logging.getLogger(__name__)


class ConsensusManager(ConsensusManagerBase):
    def from_router_statuses(self, router_statuses, valid_after=None):
        consensus = super().from_router_statuses(router_statuses, valid_after)
        logger.info("Consensus %s created", consensus)
        for rs in router_statuses:
            RouterStatus.objects.from_router_status(rs, consensus)
        logger.info("%s Router statuses created", len(router_statuses))
        return consensus


class Consensus(ConsensusBase):
    class Meta:
        ordering = ("-valid_after",)

    objects = ConsensusManager()

    def routerstatuses(self):
        return self.routerstatus_set.all()

    def routerstatuses_count(self):
        return self.routerstatus_set.count()

    def bwfiles_by_bwauth(self, bwauth):
        return self.bwfiles().get(bwauth=bwauth)

    def missing_relays_in_bwfile_by_bwauth(self, bwauth):
        return (
            self.relays()
            .difference(self.bwfiles_by_bwauth(bwauth=bwauth).relays())
            .count()
        )

    def missing_vote_relays_in_bwfile_by_bwauth(self, bwauth):
        return (
            self.relays()
            .difference(self.bwfiles_by_bwauth(bwauth=bwauth).relays())
            .count()
        )
