import logging
from datetime import datetime

from bwauthealth.models.bwauth import BwAuth
from django.contrib.postgres.fields import ArrayField
from django.db import models
from onbasca.base.models.bwfile import BwFileBase, BwFileManagerBase
from stem.descriptor.bandwidth_file import BandwidthFile

from ..util import select_bwauth

# from .bwauth import BwAuth
from .consensus import Consensus
from .relaybw import RelayBw

logger = logging.getLogger(__name__)

# KeyValues to keep for now
# This overwrites BWFILE_KEYS from onbasca.base
BWFILE_KEYS = [
    "earliest_bandwidth",
    "file_created",
    "latest_bandwidth",
    "scanner_country",
    "software_version",
    "software",
    "timestamp",
    # 'consensus_size', 'eligible_count', 'eligible_percent', 'min_count',
    # 'created_at', 'generated_at',
    # 'measurements',
    # 'min_percent',
    # "destinations_countries",
    # "time_to_report_half_network",
]


class BwFileManager(BwFileManagerBase):
    def from_bandwidth_file(self, bandwidth_file, bwauth_nickname=None):
        """Return a BwFile object from a bandwidth file.

        The bandwidth file can be a stem's BandwidthFile object or a sbws's one

        """
        # Parse bandwidth file headers
        # stem's BanwdithFile
        if isinstance(bandwidth_file, BandwidthFile):
            header = bandwidth_file
        else:
            header = bandwidth_file.header
            # in case is stem bwfile
        kwargs = dict([(k, getattr(header, k, None)) for k in BWFILE_KEYS])
        if isinstance(kwargs["timestamp"], int):
            kwargs["timestamp"] = datetime.utcfromtimestamp(
                kwargs["timestamp"]
            )

        if not kwargs["file_created"]:
            kwargs["file_created"] = kwargs.pop("timestamp")
        kwargs.pop("timestamp", None)

        if (
            RelayBw.objects.filter(
                bwfile__file_created=kwargs["file_created"]
            ).count()
            > 6000
        ):
            logger.info("Probably imported already.")
            return BwFile.objects.get(file_created=kwargs["file_created"])

        valid_after = kwargs["file_created"].replace(minute=0, second=0)
        kwargs["consensus"], _ = Consensus.objects.get_or_create(
            valid_after=valid_after
        )

        bwfile, created = self.update_or_create(
            file_created=kwargs["file_created"], defaults=kwargs
        )
        logger.info("Bwfile %s created: %s", bwfile, created)

        # Obtain the bwauth
        if not bwfile.bwauth:
            # Do not import Vote for all the file, to avoid circular dependency
            from .vote import Vote

            logger.debug("Figuring auth bwauth...")
            votes = Vote.objects.filter(
                bwfile__file_created=bwfile.file_created
            ).values_list("bwauth", flat=True)
            if votes:
                bwauth = votes[0].bwauth
            else:
                nickname = select_bwauth(
                    software=kwargs.get("software", None),
                    software_version=kwargs.get("software_version", None),
                    scanner_country=kwargs.get("scanner_country", None),
                    file_created=kwargs["file_created"],
                    # destinations_countries=kwargs["destinations_countries"]
                )
                logger.debug("Possible bwauth: %s", nickname)
                bwauth, _ = BwAuth.objects.get_or_create(nickname=nickname)
            bwfile.bwauth = bwauth
            bwfile.save()

        # Parse the bandwidth file body lines, unless the bwauth is the one
        # given by the parameter.
        # (XXXX: Because it's toflow and we don't have descriptors info there?)
        if bwauth_nickname and bwauth_nickname == bwfile.bwauth.nickname:
            logger.debug(
                "Not parsing the bandwidth file lines for bwauth %s.",
                bwauth_nickname,
            )
            return bwfile

        logger.info(
            "Parsing %s bandwidth lines", len(bandwidth_file.measurements)
        )
        bwlines_count = len(bandwidth_file.measurements.values())
        if bwfile.relaybw_set.count() >= (bwlines_count * 60 / 100):
            # No need to parse lines
            logger.debug(
                "Bandwidth file with %s Bandwidth Lines has already "
                "%s RelayBws.",
                bwlines_count,
                bwfile.relaybw_set.count(),
            )
            return bwfile
        for bwline in bandwidth_file.measurements.values():
            RelayBw.objects.from_bwfile_bwline(bwline, bwfile)
        return bwfile


class BwFile(BwFileBase):
    objects = BwFileManager()
    consensus = models.ForeignKey(
        "Consensus", null=True, blank=True, on_delete=models.CASCADE
    )
    bwauth = models.ForeignKey(
        BwAuth, null=True, blank=True, on_delete=models.SET_NULL
    )
    earliest_bandwidth = models.DateTimeField(null=True, blank=True)
    latest_bandwidth = models.DateTimeField(null=True, blank=True)

    scanner_country = models.CharField(
        max_length=2,
        null=True,
        blank=True,
    )
    # When parsing the file from an existing/published one
    _filename = models.CharField(max_length=255, null=True, blank=True)

    # Store RelayRatios, ArrayField only works with PostgreSQL
    _ratios = ArrayField(
        models.FloatField(blank=True, null=True), blank=True, null=True
    )
    _ratios_guard = ArrayField(
        models.FloatField(blank=True, null=True), blank=True, null=True
    )
    _ratios_middle = ArrayField(
        models.FloatField(blank=True, null=True), blank=True, null=True
    )
    _ratios_exit = ArrayField(
        models.FloatField(blank=True, null=True), blank=True, null=True
    )
    _ratios_guard_exit = ArrayField(
        models.FloatField(blank=True, null=True), blank=True, null=True
    )

    def relays_here_not_in(self, otherbwfile):
        self_fps = self.relaybw_set.filter(vote=1).values_list("fingerprint")
        other_fps = otherbwfile.relaybw_set.filter(vote=1).values_list(
            "fingerprint"
        )
        fps_tuple = self_fps.difference(other_fps)
        fps = [fp[0] for fp in fps_tuple]
        relays_bw = self.relaybw_set.filter(fingerprint__in=fps)
        return relays_bw

    def relaybws_relaydesc_updated_count(self):
        fps = [
            rb.fingerprint
            for rb in self.relaybw_set.all()
            if rb.is_relaydesc_updated()
        ]
        return self.relaybw_set.filter(fingerprint__in=fps).count()

    def relaybws_routerstatuses_updated_count(self):
        fps = [
            rb.fingerprint
            for rb in self.relaybw_set.all()
            if rb.is_routerstatus_updated()
        ]
        return self.relaybw_set.filter(fingerprint__in=fps).count()

    def consensus_relays_not_here(self):
        return self.consensus.routerstatus_set.values_list(
            "fingerprint", flat=True
        ).difference(self.relaybw_set.values_list("fingerprint", flat=True))

    def bw_sum(self):
        return self.relaybw_set.filter(vote=1).aggregate(models.Sum("bw"))[
            "bw__sum"
        ]

    def set_relay_balance_ratios(self):
        logger.info("Calculating ratios for bwfile %s.", self)
        # If the bwfile already has `_ratios``, no need to calculate them.
        if self._ratios:
            logger.debug(
                "%s relay ratios were already calculated for bwfile %s.",
                len(self._ratios),
                self,
            )
            return self._ratios
        relaybws = self.relaybw_set.exclude(vote=0)
        logger.info(
            "Calculating relay ratios for sbws bwfile %s with %s relay lines.",
            self,
            relaybws.count(),
        )
        ratios = []
        for rb in relaybws:
            ratio = rb.set_relay_balance_ratio()
            ratios.append(ratio)
        ratios_filtered = sorted(list(filter(lambda x: x < 3, ratios)))
        logger.debug(
            "From %s relays' ratios, %s are under 3.",
            len(ratios),
            len(ratios_filtered),
        )
        self._ratios = ratios_filtered
        self.save()
        return ratios_filtered
