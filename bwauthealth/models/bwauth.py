import logging

from django.conf import settings
from django.db import models

logger = logging.getLogger()


class BwAuthManager(models.Manager):
    def from_directory_authority(self, directory_authority):
        if directory_authority.nickname not in settings.BWAUTHS:
            return None
        bwauth, updated = self.update_or_create(
            nickname=directory_authority.nickname,
            defaults={"fingerprint": directory_authority.v3ident},
        )
        logger.info("BwAuth %s updated: %s", bwauth, updated)
        return bwauth


class BwAuth(models.Model):
    objects = BwAuthManager()
    nickname = models.CharField(max_length=19, null=True, blank=True)
    fingerprint = models.CharField(max_length=40, null=True, blank=True)
    # consensuses = models.ManyToManyField("Consensus")
    # software = models.CharField(max_length=64, null=True, blank=True)
    ipv4 = models.GenericIPAddressField(null=True, blank=True)
    # contact = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.nickname or ""

    # def recent_consensus_count(self):
    #     return self.consensuses.count()

    def relay_balance_ratios_from_bwfiles(self, dtrange=None, relay_type=None):
        ratios = []
        bwfiles = self.bwfile_set.all()
        if dtrange:
            bwfiles = bwfiles.filter(file_created__range=dtrange)
        logger.debug(
            "Calculating ratios for %s bandwidth files.", bwfiles.count()
        )
        for bwfile in bwfiles:
            ratios.extend(bwfile.set_relay_balance_ratios())
        return ratios

    def relay_balance_ratios_from_votes(self, dtrange=None, relay_type=None):
        ratios = []
        votes = self.vote_set.all()
        if dtrange:
            votes = votes.filter(valid_after__range=dtrange)
        logger.debug("Calculating ratios for %s votes.", votes.count())
        for vote in votes:
            ratios.extend(vote.set_relay_balance_ratios())
        return ratios

    def relay_balance_ratios_from_relaybalanceratios(
        self, dtrange=None, relay_type=None
    ):
        from . import RelayBalanceRatio

        ratios = []
        rbrs = RelayBalanceRatio.objects.filter(bwauth_nickname=self.nickname)
        if dtrange:
            rbrs = rbrs.filter(valid_after__range=dtrange)
        logger.debug("Calculating ratios for %s relays.", rbrs.count())
        for rbr in rbrs:
            ratios.extend(rbr.set_relay_balance_ratio())
        return ratios
