""""""
import logging
import time

from django.conf import settings
from stem.descriptor import collector

from .util import end_dt, previous_bwfile, previous_consensus

logger = logging.getLogger(__name__)


def fetch_consensuses(start=None, end=None):
    if not start:
        start = previous_consensus(settings.NOW)
    if not end:
        end = end_dt(start)
    logger.info("Fetching consensuses in period %s-%s.", start, end)
    consensuses = collector.get_consensus(
        start=start, end=end, cache_to=settings.CACHE_CONSENSUS
    )
    start_time = time.time()
    consensuses_list = list(consensuses)
    logger.debug(
        "Fetched %s consensuses in %ssecs.",
        len(consensuses_list),
        time.time() - start_time,
    )
    return consensuses_list


def fetch_relaydescs(start=None, end=None):
    if not start:
        start = previous_consensus(settings.NOW)
    if not end:
        end = end_dt(start)
    logger.info("Fetching relay descriptors in period %s-%s.", start, end)
    descriptors = collector.get_server_descriptors(
        start=start, end=end, cache_to=settings.CACHE_DESCRIPTOR
    )

    start_time = time.time()
    relaydescs_list = list(descriptors)
    logger.debug(
        "Fetched %s relay relaydescs in %ssecs.",
        len(relaydescs_list),
        time.time() - start_time,
    )
    return relaydescs_list


def fetch_bwfiles(start=None, end=None):
    if not start:
        start = previous_bwfile(settings.NOW)
    if not end:
        end = end_dt(start)
    logger.info("Fetching bandwidth decriptors in period %s-%s.", start, end)
    bwfiles = collector.get_bandwidth_files(
        start=start, end=end, cache_to=settings.CACHE_BW_FILE
    )
    start_time = time.time()
    bwfiles_list = list(bwfiles)
    logger.debug(
        "Fetched %s bandwidth descriptors in %ssecs.",
        len(bwfiles_list),
        time.time() - start_time,
    )
    return bwfiles_list
