from bwauthealth.models.relayratio import RelayRatio
from django.contrib import admin


@admin.register(RelayRatio)
class RelayRatioAdmin(admin.ModelAdmin):
    list_display = (
        "ratio",
        "relaybw",
        # "relaybw__bwfile",
    )
    list_filter = ["relaybw__bwfile"]
