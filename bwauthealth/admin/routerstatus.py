from django.contrib import admin

from bwauthealth.models.routerstatus import RouterStatus

from .links import relay_change_link


@admin.register(RouterStatus)
class RouterStatusAdmin(admin.ModelAdmin):
    list_display = (
        # "master_key_ed25519",
        "fingerprint",
        "nickname",
        # "created_at",
        "published",
        "relay_link",
        "consensus",
        "bandwidth",
        "is_unmeasured",
        "is_exit",
    )
    # ordering = ("-created_at",)
    list_filter = ["is_unmeasured", "consensus", "is_exit"]
    search_fields = ["nickname", "fingerprint"]

    def relay_link(self, obj):
        if obj.relay:
            return relay_change_link(obj.relay)
        return None

    relay_link.short_description = "Relay"
