import logging

from django.views.generic.base import TemplateView

from bwauthealth import graphs
from bwauthealth.models import BwAuth

logger = logging.getLogger(__name__)


class CdfGraph(TemplateView):
    template_name = "graph.html"

    def get_context_data(self, **kwargs):
        context = super(CdfGraph, self).get_context_data(**kwargs)
        bwauth_nickname = context.get("bwauth_nickname", None)
        start = context.get("start", None)
        end = context.get("end", None)
        if bwauth_nickname:
            logger.debug("Calculating ratios for bwauth %s.", bwauth_nickname)
            bwauth = BwAuth.objects.get(nickname=bwauth_nickname)
            figure = graphs.bwauth_relay_balance_ratio(bwauth, start, end)
        else:
            figure = graphs.bwauths_relay_balance_ratio(start, end)
        context["graph"] = figure.to_html()
        return context
