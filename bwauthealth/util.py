import argparse
import datetime
import logging

logger = logging.getLogger(__name__)


def select_bwauth(
    software=None,
    software_version=None,
    scanner_country=None,
    file_created=None,
    destinations_countries=None,
):
    # This is different depending on the period
    logger.debug("Selecting bwauth.")
    if scanner_country == "SE":
        return "maatuska"
    if scanner_country == "SG":
        return "Faravahar"
    if scanner_country == "DE":
        return "gabelmoo"
    if scanner_country == "US":
        if file_created.minute <= 35:
            return "longclaw"
        else:
            return "bastet"
    return "None"


def valid_datetime(s):
    try:
        return datetime.datetime.fromisoformat(s)
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def dtrange_split(s):
    try:
        start, end = map(valid_datetime, s.split(","))
        return start, end
    except:
        raise argparse.ArgumentTypeError("Datetime dtrange must be x,y")


def dt2isostr(dt):
    return dt.replace(microsecond=0, second=0).isoformat()


def dtrange2isostr(dtrange):
    return "({},{})".format(*tuple(map(lambda x: dt2isostr(x), dtrange)))


def dtranges2isostr(dtranges):
    return ", ".join([dtrange2isostr(dtrange) for dtrange in dtranges])


def previous_consensus(start_dt):
    """
    The last/previous consensus, generated at 00, are usually available at
    collector 5min later. To ensure it's the last from previous hours
    (cause bwfiles can take 2h to be available), take starting from 3 hours
    before at min 5.

    """
    return (start_dt - datetime.timedelta(hours=4)).replace(
        minute=55, second=0, microsecond=0
    )


def previous_bwfile(start_dt):
    """
    The bwfiles are published after the consensus and some are available only
    2h later.
    Currently, they are generated at the time of the 1st column and published
    in collector at the time of the 2nd column:
    - 2020-11-17-06-05-19, 2020-11-17 07:35
    - 2020-11-17-06-37-23, 2020-11-17 07:35
    - 2020-11-17-06-38-22, 2020-11-17 07:35
    - 2020-11-17-06-43-37, 2020-11-17 08:35
    - 2020-11-17-06-43-37, 2020-11-17 07:36
    - 2020-11-17-06-44-40, 2020-11-17 07:35
    - 2020-11-17-06-46-44, 2020-11-17 08:35

    XXX: Do they all correspond to the consensus generated 2020-11-17 06:00?

    """
    return (start_dt - datetime.timedelta(hours=3)).replace(
        minute=5, second=0, microsecond=0
    )


def end_dt(start_dt):
    return start_dt + datetime.timedelta(hours=1)
